﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;

using System.Data;
using Core.Models;
using Data.Repository;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "Service1" in both code and config file together.
    public class Service1 : IService1
    {
        private readonly CompanyRepository _companyRepository = new CompanyRepository();
        private readonly ModelRepository _modelRepository = new ModelRepository();
        private readonly CarRepository _carRepository = new CarRepository();
        private readonly SpareRepository _spareRepository = new SpareRepository();
        
        public string GetData(int value)
        {
            return string.Format("You entered: {0}", value);
        }

        public CompositeType GetDataUsingDataContract(CompositeType composite)
        {
            if (composite == null)
            {
                throw new ArgumentNullException("composite");
            }
            if (composite.BoolValue)
            {
                composite.StringValue += "Suffix";
            }
            return composite;
        }

        //Gen

        //Company
        public Company GetCompany(long id)
        {
            return _companyRepository.Get.FirstOrDefault(x => x.Id == id);
        }

        public List<Company> GetAllCompanies()
        {
            var a = _companyRepository.Get.ToList();
            return a;
        }

        public void SaveCompany(Company company)
        {
            _companyRepository.Save(company);
        }

        public void DeleteCompany(Company company)
        {
            var a = _companyRepository.Get.FirstOrDefault(x => x.Id == company.Id); // ?!
            _companyRepository.Remove(a);
        }


        //Model
        public Model GetModel(long id)
        {
            return MapModel(_modelRepository.Get.FirstOrDefault(x => x.Id == id));;
        }

        public List<Model> GetAllModels()
        {
            var modelsProxy = _modelRepository.Get.Where(x => x.Company != null).ToList();
            var models = new List<Model>();
            
            foreach (var model in modelsProxy)
                models.Add(MapModel(model));
            
            return models;
        }

        public void SaveModel(Model model)
        {
            _modelRepository.Save(model);
        }

        public void DeleteModel(Model model)
        {
            _modelRepository.Remove(_modelRepository.Get.FirstOrDefault(x => x.Id == model.Id));
        }


        //Car
        public Car GetCar(long id)
        {
            var car = _carRepository.Get.FirstOrDefault(x => x.Id == id);
            return _carRepository.Get.FirstOrDefault(x => x.Id == id);
        }

        public List<Car> GetAllCars()
        {
            var carsProxy = _carRepository.Get.Where(x => x.Model != null && x.Model.Company != null);
            var cars = new List<Car>();

            foreach (var car in carsProxy)
            {
                cars.Add(new Car()
                {
                    Model = new Model()
                    {
                        Name = car.Model.Name,
                        Company = car.Model.Company
                    },

                    Number = car.Number,
                    Owner = car.Owner
                });
            }

            return cars;
        }

        public void SaveCar(Car car)
        {
            _carRepository.Save(car);
        }

        public void DeleteCar(Car car)
        {
            var a = _carRepository.Get.FirstOrDefault(x => x.Id == car.Id); // ?!
            _carRepository.Remove(car);
        }


        //Spare
        public Spare GetSpare(long id)
        {
            return _spareRepository.Get.FirstOrDefault(x => x.Id == id);
        }

        public List<Spare> GetAllSpares()
        {
            var a = _spareRepository.Get.ToList();

            return a;
        }

        public void SaveSpare(Spare spare)
        {
            
            
            _spareRepository.Save(spare);
        }

        public void DeleteSpare(Spare spare)
        {
            _spareRepository.Remove(spare);
        }


        //Map

        //Model map
        private Model MapModel(Model model)
        {
            return new Model()
                {
                    Id = model.Id,
                    Name = model.Name,
                    Company = model.Company
                };
        }
    }

    
}
