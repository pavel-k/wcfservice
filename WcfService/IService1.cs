﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using Core.Models;

namespace WcfService
{
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IService1" in both code and config file together.
    [ServiceContract]
    public interface IService1
    {
        [OperationContract]
        string GetData(int value);

        [OperationContract]
        CompositeType GetDataUsingDataContract(CompositeType composite);

        //Company
        [OperationContract]
        Company GetCompany(long id);

        [OperationContract]
        List<Company> GetAllCompanies();

        [OperationContract]
        void SaveCompany(Company company);

        [OperationContract]
        void DeleteCompany(Company company);

        //Model
        [OperationContract]
        Model GetModel(long id);

        [OperationContract]
        List<Model> GetAllModels();

        [OperationContract]
        void SaveModel(Model model);

        [OperationContract]
        void DeleteModel(Model model);
        
        //Car
        [OperationContract]
        Car GetCar(long id);

        [OperationContract]
        List<Car> GetAllCars();

        [OperationContract]
        void SaveCar(Car car);

        [OperationContract]
        void DeleteCar(Car car);

        //Spare
        [OperationContract]
        Spare GetSpare(long id);

        [OperationContract]
        List<Spare> GetAllSpares();

        [OperationContract]
        void SaveSpare(Spare spare);

        [OperationContract]
        void DeleteSpare(Spare spare);


    }

    // Use a data contract as illustrated in the sample below to add composite types to service operations.
    // You can add XSD files into the project. After building the project, you can directly use the data types defined there, with the namespace "WcfService.ContractType".
    [DataContract]
    public class CompositeType
    {
        bool boolValue = true;
        string stringValue = "Hello ";

        [DataMember]
        public bool BoolValue
        {
            get { return boolValue; }
            set { boolValue = value; }
        }

        [DataMember]
        public string StringValue
        {
            get { return stringValue; }
            set { stringValue = value; }
        }
    }
}
